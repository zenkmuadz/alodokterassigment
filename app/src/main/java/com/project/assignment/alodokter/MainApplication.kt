package com.project.assignment.alodokter

import android.app.Application
import android.content.Context
import androidx.multidex.MultiDex
import androidx.multidex.MultiDexApplication
import com.project.rantingservice.admin.di.sessionModule
import com.project.rantingservice.admin.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import java.io.PrintWriter
import java.io.StringWriter
import kotlin.system.exitProcess

class MainApplication : MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@MainApplication)
            modules(sessionModule)
            modules(viewModelModule)
        }

        Thread.setDefaultUncaughtExceptionHandler(ExceptionHandler())

    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }
}

class ExceptionHandler : Thread.UncaughtExceptionHandler {

    override fun uncaughtException(thread: Thread, exception: Throwable) {
        val stackTrace = StringWriter()
        exception.printStackTrace(PrintWriter(stackTrace))

        val errorReport = StringBuilder()
        errorReport.append(stackTrace.toString())

        //Timber.e(errorReport.toString())

        android.os.Process.killProcess(android.os.Process.myPid())
        exitProcess(10)
    }

}
