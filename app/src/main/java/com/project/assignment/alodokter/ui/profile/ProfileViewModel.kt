package com.project.assignment.alodokter.ui.profile

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.project.assignment.alodokter.base.BaseViewModel
import com.project.assignment.alodokter.datamodel.ProfileModel
import com.project.assignment.alodokter.utils.Constant

class ProfileViewModel : BaseViewModel() {

    val successProfile = MutableLiveData<ProfileModel>()

    fun callProfile () {
        successProfile.value = Constant.getDataProfile()
    }
}