package com.project.assignment.alodokter.ui.home

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.project.assignment.alodokter.R
import com.project.assignment.alodokter.base.BaseFragment
import com.project.assignment.alodokter.datamodel.ImageModel
import kotlinx.android.synthetic.main.fragment_home.*
import org.koin.android.ext.android.inject

class HomeFragment : BaseFragment() {
    private val vm: HomeViewModel by inject()
    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_home, container, false)

        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRequest()
        initObserve()
    }

    private fun initRequest() {
        vm.callGetImage()
    }

    private fun initObserve(){
        vm.successImage.observe(requireActivity(), Observer {
            setData(it)
        })
    }

    private fun setData(it: ArrayList<ImageModel>) {
        val adapter = HomeAdapter(it)
        adapter.setInterface(object: HomeAdapter.HomeAdapterInterface{
            override fun onClick(data: ImageModel) {
                val i = Intent(requireContext(), DetailActivity::class.java)
                i.putExtra(DETAIL_IMAGE, data)
                startActivity(i)
            }

        })
        reclist.adapter = adapter
    }

    companion object {
        val DETAIL_IMAGE = "DETAIL_IMAGE"
    }
}