package com.project.rantingservice.admin.service.session

import android.content.Context
import android.content.SharedPreferences
import com.project.assignment.alodokter.utils.Constant

class SessionManager (context: Context) {
    private var pref: SharedPreferences
    private var editor: SharedPreferences.Editor

    init {
        pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE)
        editor = pref.edit()
    }

    var sessionIsLogin: Boolean
        get() = pref.getBoolean(Constant.IS_LOGIN, false)
        set(sessionIsLogin) {
            editor.putBoolean(Constant.IS_LOGIN, sessionIsLogin)
            editor.commit()
        }

    companion object {
        private const val PREF_NAME = "SessionManagerAlodokter"
        private const val PRIVATE_MODE = 0
    }
}
