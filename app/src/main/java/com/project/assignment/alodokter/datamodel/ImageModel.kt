package com.project.assignment.alodokter.datamodel

import android.os.Parcelable
import java.io.Serializable

class ImageModel : Serializable {
    var id: Int = 0
    var image: Int = 0
    var image_detail: ArrayList<Int> = ArrayList()
}