package com.project.assignment.alodokter.ui.home

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.project.assignment.alodokter.R
import com.project.assignment.alodokter.datamodel.ImageModel
import kotlinx.android.synthetic.main.layout_list.view.*
import java.util.*
import kotlin.collections.ArrayList

class HomeAdapter(private val list: ArrayList<ImageModel>):
        RecyclerView.Adapter<HomeAdapter.ItemViewHolder>() {

    private lateinit var context: Context
    private var listener: HomeAdapterInterface? = null

    fun setInterface (callback: HomeAdapterInterface) {
        listener = callback
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val view: View = LayoutInflater.from(parent.context)
                .inflate(R.layout.layout_list, parent, false)
        context = parent.context
        return ItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val data = list[position]
        showImageFromUrlWithGlide(data.image, holder.image)
        holder.image.setOnClickListener {
            listener!!.onClick(data)
        }
    }

    private fun showImageFromUrlWithGlide(link: Int, imageView: ImageView) {
        val option = RequestOptions()
                .centerCrop()
                .error(R.drawable.ic_launcher_background)

        Glide.with(context)
                .load(link)
                .apply(option)
                .into(imageView)
    }

    inner class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var image = itemView.image
    }

    interface HomeAdapterInterface {
        fun onClick(data: ImageModel)
    }

}
