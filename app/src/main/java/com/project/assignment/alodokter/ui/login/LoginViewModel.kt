package com.project.assignment.alodokter.ui.login

import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.project.assignment.alodokter.R
import com.project.assignment.alodokter.base.BaseViewModel
import com.project.rantingservice.admin.service.session.SessionManager
import java.lang.ref.WeakReference

class LoginViewModel(val context: Context, val session: SessionManager) : BaseViewModel() {

    var successLogin = MutableLiveData<Boolean>()
    var emailError = MutableLiveData<String>()
    var passwordError = MutableLiveData<String>()

    fun callLogin(email: String, password: String) {
        if (checkData(email, password)) {
            session.sessionIsLogin = true
            successLogin.value = true
        }
    }

    private fun checkData (email: String, password: String) : Boolean {
        if (email.isEmpty()) {
            emailError.value = context.getString(R.string.err_no_email)
            return false
        }
        if (password.isEmpty()) {
            passwordError.value =  context.getString(R.string.err_no_password)
            return false
        }

        return true
    }

}