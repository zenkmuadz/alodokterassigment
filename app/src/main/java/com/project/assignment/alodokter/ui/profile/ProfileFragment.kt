package com.project.assignment.alodokter.ui.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.project.assignment.alodokter.R
import com.project.assignment.alodokter.base.BaseFragment
import com.project.assignment.alodokter.datamodel.ProfileModel
import kotlinx.android.synthetic.main.fragment_profile.*
import org.koin.android.ext.android.inject

class ProfileFragment : BaseFragment() {
    val vm: ProfileViewModel by inject()
    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_profile, container, false)

        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRequest()
        initObserver()
    }

    private fun initRequest() {
        vm.callProfile()
    }

    private fun initObserver () {
        vm.successProfile.observe(requireActivity(), Observer {
            setData(it)
        })
    }

    private fun setData(it: ProfileModel) {
        txtName.text = it.name
        txtGender.text = it.gender
        txtNotelp.text = it.nohp
    }
}