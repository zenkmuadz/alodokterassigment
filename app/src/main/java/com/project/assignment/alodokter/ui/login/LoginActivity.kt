package com.project.assignment.alodokter.ui.login

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import com.project.assignment.alodokter.R
import com.project.assignment.alodokter.base.BaseActivity
import com.project.assignment.alodokter.ui.MainActivity
import kotlinx.android.synthetic.main.activity_login.*
import org.koin.android.ext.android.inject

class LoginActivity : BaseActivity() {
    private val vm: LoginViewModel by inject()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        initObserve()
        initListener()
    }

    private fun initObserve() {
        vm.successLogin.observe(this, Observer {
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        })
        vm.emailError.observe(this, Observer {
            inputEmail.error = it
        })
        vm.passwordError.observe(this, Observer {
            inputPassword.error = it
        })
    }

    private fun initListener() {
        btnLogin.setOnClickListener {
            val email = inputEmail.text.toString().trim()
            val password = inputPassword.text.toString().trim()
            vm.callLogin(email, password)
        }
    }
}