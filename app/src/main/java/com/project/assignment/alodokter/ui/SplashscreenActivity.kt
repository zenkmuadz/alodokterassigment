package com.project.assignment.alodokter.ui

import android.content.Intent
import android.os.Handler
import android.os.Bundle
import androidx.lifecycle.Observer
import com.project.assignment.alodokter.R
import com.project.assignment.alodokter.base.BaseActivity
import com.project.assignment.alodokter.ui.login.LoginActivity

class SplashscreenActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splashscreen)
        checkLogin()
    }

    private fun checkLogin() {
        Handler().postDelayed({
            if (session.sessionIsLogin) {
                val intent = Intent(this@SplashscreenActivity, MainActivity::class.java)
                startActivity(intent)
                finish()
            } else {
                val intent = Intent(this@SplashscreenActivity, LoginActivity::class.java)
                startActivity(intent)
                finish()
            }
        }, 1000)
    }

}
