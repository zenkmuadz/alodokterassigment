package com.project.assignment.alodokter.base

import android.app.Activity
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.ViewModel
import org.koin.core.KoinComponent
import org.koin.core.inject

abstract class BaseViewModel : ViewModel(), KoinComponent, LifecycleObserver {

    protected val TAG = javaClass.simpleName

    override fun onCleared() {

    }

}