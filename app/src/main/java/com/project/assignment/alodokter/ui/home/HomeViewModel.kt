package com.project.assignment.alodokter.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.project.assignment.alodokter.base.BaseViewModel
import com.project.assignment.alodokter.datamodel.ImageModel
import com.project.assignment.alodokter.utils.Constant

class HomeViewModel : BaseViewModel() {

    var successImage = MutableLiveData<ArrayList<ImageModel>>()

    fun callGetImage () {
        successImage.value = Constant.getDataImage()
    }
}