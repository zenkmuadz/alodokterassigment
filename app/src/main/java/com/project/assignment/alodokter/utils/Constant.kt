package com.project.assignment.alodokter.utils

import com.project.assignment.alodokter.R
import com.project.assignment.alodokter.datamodel.ImageModel
import com.project.assignment.alodokter.datamodel.ProfileModel
import java.util.*
import kotlin.collections.ArrayList

object Constant {

    const val EMAIL = "EMAIL"

    const val IS_LOGIN = "IS_LOGIN"

    fun getDataImage () : ArrayList<ImageModel> {
        val data = ArrayList<ImageModel>()
        val image1 = ImageModel()
        image1.id = 1
        image1.image = R.drawable.image1_1
        image1.image_detail = arrayListOf(R.drawable.image1_1,
            R.drawable.image1_2,
            R.drawable.image1_3)

        val image2 = ImageModel()
        image2.id = 2
        image2.image = R.drawable.image2_1
        image2.image_detail = arrayListOf(R.drawable.image2_1,
            R.drawable.image2_2,
            R.drawable.image2_3)

        val image3 = ImageModel()
        image3.id = 3
        image3.image = R.drawable.image3_1
        image3.image_detail = arrayListOf(R.drawable.image3_1,
            R.drawable.image3_2,
            R.drawable.image3_3)

        data.add(image1)
        data.add(image2)
        data.add(image3)

        return data
    }

    fun getDataProfile () : ProfileModel {
        val data = ProfileModel()
        data.name = "Rakka Purnama"
        data.gender = "Male"
        data.nohp = "0987567458574"

        return data
    }

}
