package com.project.rantingservice.admin.di

import com.project.assignment.alodokter.ui.home.HomeViewModel
import com.project.assignment.alodokter.ui.login.LoginViewModel
import com.project.assignment.alodokter.ui.profile.ProfileViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {

    viewModel { HomeViewModel() }
    viewModel { LoginViewModel(get(), get()) }
    viewModel { ProfileViewModel() }
}