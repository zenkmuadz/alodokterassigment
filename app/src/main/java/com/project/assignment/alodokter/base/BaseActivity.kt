package com.project.assignment.alodokter.base

import android.annotation.SuppressLint
import android.content.pm.ActivityInfo
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.project.rantingservice.admin.service.session.SessionManager
import org.koin.android.ext.android.inject

open class BaseActivity : AppCompatActivity() {
    val session: SessionManager by inject()

    @SuppressLint("SourceLockedOrientationActivity")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initScreen()

    }

    private fun initScreen(){
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LOCKED
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
    }


}
