package com.project.assignment.alodokter.ui.home

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import androidx.lifecycle.Observer
import com.project.assignment.alodokter.R
import com.project.assignment.alodokter.base.BaseActivity
import com.project.assignment.alodokter.datamodel.ImageModel
import com.project.assignment.alodokter.ui.MainActivity
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType
import com.smarteist.autoimageslider.SliderAnimations
import com.smarteist.autoimageslider.SliderView
import kotlinx.android.synthetic.main.activity_detail.*
import org.koin.android.ext.android.inject

class DetailActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        setDetail()
    }

    private fun setDetail () {
        val data = intent.extras!!.get(HomeFragment.DETAIL_IMAGE) as ImageModel
        val adapter = DetailAdapter(data.image_detail)

        imageSlider.setSliderAdapter(adapter)
        imageSlider.setIndicatorAnimation(IndicatorAnimationType.WORM)
        imageSlider.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION)
        imageSlider.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH)
        imageSlider.setIndicatorSelectedColor(Color.WHITE)
        imageSlider.setIndicatorUnselectedColor(Color.GRAY)
        imageSlider.setScrollTimeInSec(4) //set scroll delay in seconds :
        imageSlider.startAutoCycle()
    }
}