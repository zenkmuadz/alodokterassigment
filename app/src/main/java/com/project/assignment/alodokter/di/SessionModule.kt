package com.project.rantingservice.admin.di

import com.project.rantingservice.admin.service.session.SessionManager
import org.koin.dsl.module

val sessionModule = module {

    single { SessionManager(get()) }

}