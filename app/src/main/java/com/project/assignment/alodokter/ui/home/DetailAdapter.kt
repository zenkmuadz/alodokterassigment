package com.project.assignment.alodokter.ui.home

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.project.assignment.alodokter.R
import com.smarteist.autoimageslider.SliderViewAdapter
import kotlinx.android.synthetic.main.layout_list_detail.view.*

class DetailAdapter(private val list: ArrayList<Int>):
    SliderViewAdapter<DetailAdapter.ResiItemViewHolder>() {

    private lateinit var context: Context

    override fun getCount(): Int {
        return list.size
    }

    override fun onCreateViewHolder(parent: ViewGroup?): ResiItemViewHolder {
        context = parent!!.context
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.layout_list_detail, parent, false)
        return ResiItemViewHolder(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ResiItemViewHolder, position: Int) {
        showImageFromUrlWithGlide(list.get(position), holder.image)
    }

    private fun showImageFromUrlWithGlide(link: Int, imageView: ImageView) {
        val option = RequestOptions()
                .error(R.drawable.ic_launcher_background)

        Glide.with(context)
                .load(link)
                .apply(option)
                .into(imageView)
    }

    inner class ResiItemViewHolder(itemView: View) : SliderViewAdapter.ViewHolder(itemView) {
        internal var image = itemView.image

    }

}
